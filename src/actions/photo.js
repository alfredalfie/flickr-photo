import jsonp from 'jsonp';

export const SEARCH_PHOTOS_SENDING = 'SEARCH_PHOTOS_SENDING';
export const SEARCH_PHOTOS_SUCCESS = 'SEARCH_PHOTOS_SUCCESS';
export const SEARCH_PHOTOS_ERROR = 'SEARCH_PHOTOS_ERROR';

export function searchError(bool) {
  return {
    type: SEARCH_PHOTOS_ERROR,
    hasError: bool,
  };
}

export function isSearching(bool) {
  return {
    type: SEARCH_PHOTOS_SENDING,
    isSearching: bool,
  };
}

export function searchSuccess(photos) {
  return {
    type: SEARCH_PHOTOS_SUCCESS,
    photos,
  };
}

export const search = (term = '') => 
  (dispatch) => {
    dispatch(isSearching(true));

    const url = `https://api.flickr.com/services/feeds/photos_public.gne?format=json&tags=${term}`;

    jsonp(url, {
      name: 'jsonFlickrFeed',
    }, (err, photos) => {
      if (err) {
        return dispatch(searchError(true));
      }
      return dispatch(searchSuccess(photos));
    });
  }
  
