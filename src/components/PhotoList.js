import React from 'react';
const TagList = ({tags, onClickTag}) => {
  const tagItems = tags.map(tag => {
    return (
      <li className="tag-item" key={tag} onClick={() => onClickTag(tag)}>
        <span>#{tag}</span>
      </li>
    )
  });
  return (
    <ul className="tag-list list-unstyled clearfix">
      {tagItems}
    </ul>
  );
};

const PhotoList = ({items, onClickTag}) => {
  if (!items || items.length < 1) {
    return <div>No photo</div>
  }

  const photos = items.map(item => {
    return (
      <li className="block-photo" key={item.link}>
        <div className="inner">
          <div className="thumb">
            <a href={item.link} target="_blank">
              <img src={item.media.m} alt={item.title}/>
            </a>
          </div>
          <div className="author">
            <h4>{item.author}</h4>
          </div>

          <div className="hires-link box">
            <a href={item.link} target="_blank">View image at hi-res</a>
          </div>
          
          <TagList tags={item.tags.split(' ')} onClickTag={onClickTag}/>
          
        </div>
      </li>
    )
  });

  return (
    <div className="panel">
      <ul className="block-list list-unstyled clearfix">
        {photos}
      </ul>
    </div>
  );
};

export default PhotoList;