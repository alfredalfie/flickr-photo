import { combineReducers } from 'redux';
import photos from './photo';

const reducers = combineReducers(Object.assign(
  {}, 
  photos,
));

export default reducers;