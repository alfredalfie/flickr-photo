import { SEARCH_PHOTOS_SENDING, SEARCH_PHOTOS_SUCCESS, SEARCH_PHOTOS_ERROR } from '../actions/photo';

const searchError = (state = false, action) => {
  switch (action.type) {
    case SEARCH_PHOTOS_ERROR:
      return action.hasError;

    default:
      return state;
  }
}

const isSearching = (state = false, action) => {
  switch (action.type) {
    case SEARCH_PHOTOS_SENDING:
      return action.isSearching;
    case SEARCH_PHOTOS_SUCCESS:
    case SEARCH_PHOTOS_ERROR:
      return false;
    
    default:
      return state;
  }
}

const photos = (state = [], action) => {
  switch (action.type) {
    case SEARCH_PHOTOS_SUCCESS: 
      return action.photos.items;
    
    default:
      return state;
  }
};

export default {
  searchError,
  isSearching,
  photos,
};

