import React from 'react';
import ReactDOM from 'react-dom';

import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';
import loggerMiddleware from 'redux-logger';

import './index.css';
import App from './App';
import reducers from './reducers';

import registerServiceWorker from './registerServiceWorker';

const middleware = compose(
  applyMiddleware(
    thunkMiddleware,
    loggerMiddleware,
  )
);

const store = createStore(reducers, middleware);



ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root'));
registerServiceWorker();
