import React, { Component } from 'react';
import PropTypes from 'prop-types';

export class SearchBar extends Component {

  constructor(props) {
    super(props);

    this.state = { term: '' };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.selectedTag !== nextProps.selectedTag) {
      this.setState({ term: nextProps.selectedTag });
    }
    
  }

  render() {
    return (
      <div className="search-bar">
        <input
          value={this.state.term}
          onChange={event => this.onInputChange(event.target.value)}
        />
      </div>
    );
  }

  onInputChange(term) {
    this.setState({term});
    this.props.onSearchTermChange(term);
  }
}

SearchBar.propTypes = {
  onSearchTermChange: PropTypes.func,
};


export default SearchBar;