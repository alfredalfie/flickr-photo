import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import _ from 'lodash';

import './App.css';

import SearchBar from './components/SearchBar';
import PhotoList from './components/PhotoList';

import { search } from './actions/photo';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedTag: '',
    };
  }

  componentDidMount() {
    this.photoSearch('surfboard');
  }

  photoSearch(term) {
    this.props.search(term);
  }

  onClickTag(tag) {
    this.setState({ selectedTag: tag });
    this.props.search(tag);
  }

  render() {
    const photoSearch = _.debounce(term => this.photoSearch(term), 300);

    return (
      <div className="container">
        <SearchBar onSearchTermChange={photoSearch} selectedTag={this.state.selectedTag} />
        {
          this.props.isSearching &&
          <div>Loading...</div>
        }
        <PhotoList items={this.props.photos} onClickTag={this.onClickTag.bind(this)}/>
      </div>
    );
  }
}

App.propTypes = {
  photos: PropTypes.array,
  search: PropTypes.func,
};

/* Container */
const mapStateToProps = (state) => ({
  searchError: state.searchError,
  isSearching: state.isSearching,
  photos: state.photos
});

const mapDispatchToProps = (dispatch) => ({
  search: (term) => {
    dispatch(search(term));
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(App);

// export default App;
